# Multi language support 4 XPages demo database #
A demo database to show my way to handle and add multi language support at an XPages application.
## Requirements ##

To run this application there are some requirements

* Domino 9.0.1
+ OpenNTF Essentials, http://essentials.openntf.org/
    * Domino API, to simplify the Java code
